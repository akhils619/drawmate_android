package com.akhil.drawmate.variables;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Vector;

public class Variables {
    public static int timesPlayed = 1;
    public static int correctGuesses = 0;
    public static Vector<String> alreadyDone = new Vector<>(8);
    public static String[] voidChallenges = {"Umbrella", "Envelope", "Lollipop", "Pencil", "Circle", "Knife", "Boomerang",
            "Cloud", "Windmill", "Sun", "Stairs", "Traffic Light", "Ladder", "Chair", "Cup", "Pants", "Light Bulb", "Cake",
            "Apple", "Snowman", "Vase", "Triangle", "Line", "Mountain", "Star"};

}
