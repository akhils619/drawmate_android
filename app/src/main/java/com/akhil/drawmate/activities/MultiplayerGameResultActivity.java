package com.akhil.drawmate.activities;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.akhil.drawmate.R;
import com.akhil.drawmate.helpers.GameHelper;
import com.akhil.drawmate.models.Player;
import com.akhil.drawmate.variables.Variables;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MultiplayerGameResultActivity extends AppCompatActivity {

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference users = mDatabase.child("users");
    private DatabaseReference gameData = mDatabase.child("gamedata");

    public int playerOneScore;
    public int playerTwoScore;
    private TextView winner_textView;
    private String playerName;
    private String gameRoomKey;
    private String userKey;
    private String player1Name;
    private String player2Name;
    public GameHelper gameHelper;
    private boolean isHost;
    SharedPreferences preferences;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_result);

        gameHelper = GameHelper.getInstance(this);
        TextView displayView = findViewById(R.id.numGuess);
        winner_textView = findViewById(R.id.winner_text);
        int CHALLENGE_COUNT = 4;
        String set = "Guessed " + Variables.correctGuesses + " / "+ CHALLENGE_COUNT +  "correctly!";

        displayView.setText(set);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        isHost = preferences.getBoolean("isHost", false);
        playerName = preferences.getString("playerName", "");
        gameRoomKey = preferences.getString("gameRoomKey", "");

        users.push().setValue(new Player(playerName, Variables.correctGuesses, isHost));
        gameData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.i("MultiplayerGameResult", "GameRoomKeyInsideloop" + gameRoomKey);
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Log.i("MultiplayerGameResult", "GameRoomKeyInsideloop" + gameRoomKey);
                    if (ds.getKey().equalsIgnoreCase(gameRoomKey)) {
                        player1Name = ds.child("player1").getValue().toString();
                        player2Name = ds.child("player2").getValue().toString();
                        Log.i("MultiplayerGameResult", "Players1" + player1Name);
                        Log.i("MultiplayerGameResult", "Players2" + player2Name);
                        if (player1Name != null && player2Name != null) {
                            return;
                        }
                    }
                }
                Log.i("GameResultActivitout", "Players1" + player1Name);
                Log.i("GameResultActivityout", "Players2" + player2Name);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Button showWinnerBtn = findViewById(R.id.show_winner);
        showWinnerBtn.setVisibility(View.VISIBLE);
        winner_textView.setVisibility(View.VISIBLE);
        showWinnerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("MultiplayerGameResult", "Show Winner button clicked");
                users.addValueEventListener(new ValueEventListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            if (ds.child("name").getValue().toString().equalsIgnoreCase(player1Name)) {
                                userKey=ds.getKey();
                                playerOneScore = Integer.parseInt(ds.child("score").getValue().toString());
                            } else if (ds.child("name").getValue().toString().equalsIgnoreCase(player2Name)) {
                                userKey=ds.getKey();
                                playerTwoScore = Integer.parseInt(ds.child("score").getValue().toString());
                            }

                        }
                        if (playerOneScore > 0 || playerTwoScore > 0) {
                            findWinner();
                            return;
                        } else {
                            winner_textView.setText("Game Tied");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }


        });

        Button playAgain = findViewById(R.id.playAgain);

        playAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameData.child(gameRoomKey).removeValue();
                users.child(userKey).removeValue();
                Variables.timesPlayed = 1;
                Variables.correctGuesses = 0;
                Variables.alreadyDone.clear();
                startActivity(new Intent(MultiplayerGameResultActivity.this, MainMenuActivity.class));
            }
        });

        Button gotoMainMenu = findViewById(R.id.goToMenu);
        gotoMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameData.child(gameRoomKey).removeValue();
                users.child(userKey).removeValue();
                startActivity(new Intent(MultiplayerGameResultActivity.this, MainMenuActivity.class));
            }
        });
    }

    public void findWinner() {
        Log.i(" ", "Inside find Winner");
        Log.i("MultiplayerGameResult", "Players1Score" + playerOneScore);
        Log.i("MultiplayerGameResult", "Players2Score" + playerTwoScore);

        if (player1Name.equalsIgnoreCase(playerName) && playerOneScore > playerTwoScore) {
            winner_textView.setText("You won the game!");
        } else if (player2Name.equalsIgnoreCase(playerName) && playerTwoScore > playerOneScore) {
            winner_textView.setText("You won the game");
        } else if (player1Name.equalsIgnoreCase(playerName) && playerTwoScore > playerOneScore) {
            winner_textView.setText(player2Name + " won the game");
        } else if (player2Name.equalsIgnoreCase(playerName) && playerOneScore > playerTwoScore) {
            winner_textView.setText(player1Name + " won the game");
        } else if (playerTwoScore == playerOneScore) {
            winner_textView.setText("Game tied");
        }
    }
}