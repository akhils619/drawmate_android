package com.akhil.drawmate.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.akhil.drawmate.R;
import com.akhil.drawmate.helpers.GameHelper;
import com.akhil.drawmate.models.GameData;
import com.akhil.drawmate.models.Player;
import com.akhil.drawmate.variables.Variables;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class PlayerSelectionActivity extends AppCompatActivity {


    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference gameData = mDatabase.child("gamedata");
    public Player player;
    public String gameRoomKey;
    public ArrayList<String> gameChallenges;
    public GameHelper gameHelper;
    private boolean isGameRoomCreated = false;
    public boolean isHost;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_selection);

        final Button saveBtn = findViewById(R.id.save_btn);
        final Button participantButton = findViewById(R.id.id_participant);
        final Button hostButton = (Button) findViewById(R.id.id_host);
        final Button startGameBtn = findViewById(R.id.startGame);
        final EditText playerName = findViewById(R.id.playerName_editText);
        gameHelper = GameHelper.getInstance(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        isGameRoomCreated = false;
        startGameBtn.setVisibility(View.INVISIBLE);

        hostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("PlayerSelectionActivity", "Host button clicked");
                isHost = true;
                gameChallenges = gameHelper.multiplayerLabels;
                participantButton.setEnabled(false);
                participantButton.setBackgroundColor(Color.GRAY);

                gameData.push().setValue(new GameData(player.getName(), "", "waiting", 0, 0, 0, 0, gameChallenges));
                gameData.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.i("PlayerSelectionActivity", "Inside data change");
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            if (ds.child("player1").getValue().toString().equalsIgnoreCase(player.getName())) {
                                if (!ds.child("player2").getValue().toString().equalsIgnoreCase("")) {
                                    gameRoomKey = ds.getKey().toString();
                                    startGameBtn.setVisibility(View.VISIBLE);
                                    if (ds.child("gameStatus").getValue().toString().equalsIgnoreCase("Started")) {
                                        Log.i("PlayerSelectionActivity", "GameStatus" + " " + "Started");
                                        Intent begin = new Intent(PlayerSelectionActivity.this, MultiplayerStartGameActivity.class);
                                        editor.putBoolean("isMultiplayer", true);
                                        editor.putBoolean("isHost", true);
                                        editor.putString("playerName", player.getName());
                                        editor.putString("gameRoomKey", gameRoomKey);
                                        editor.putStringSet("gameChallenges", new HashSet<String>(gameChallenges));
                                        editor.commit();
                                        startActivity(begin);
                                    }
                                }
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

        });


        participantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("PlayerSelectionActivity", "Participant button clicked");
                hostButton.setEnabled(false);
                hostButton.setBackgroundColor(Color.GRAY);
                gameChallenges = new ArrayList<String>();
                gameData.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.i("PlayerSelectionActivity", "Inside data change");
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            if (ds.child("player2").getValue().toString().equalsIgnoreCase("")) {
                                if (ds.child("gameStatus").getValue().toString().equalsIgnoreCase("waiting")) {
                                    gameRoomKey = ds.getKey();
                                    gameData.child(ds.getKey()).child("player2").setValue(player.getName());
                                    editor.putString("playerName", player.getName());

                                    gameChallenges.add(ds.child("gameChallenges").child("0").getValue().toString());
                                    gameChallenges.add(ds.child("gameChallenges").child("1").getValue().toString());
                                    gameChallenges.add(ds.child("gameChallenges").child("2").getValue().toString());
                                    gameChallenges.add(ds.child("gameChallenges").child("3").getValue().toString());
                                    gameChallenges.add(ds.child("gameChallenges").child("4").getValue().toString());
                                    gameChallenges.add(ds.child("gameChallenges").child("5").getValue().toString());

                                    gameHelper.setLabels(gameChallenges);
                                    isGameRoomCreated = true;

                                    Log.i("PlayerSelectionActivity", "GameChallenges" + " " + gameChallenges.size());
                                    Log.i("PlayerSelectionActivity", "GameRoom created" + " " + isGameRoomCreated);
                                    return;
                                }
                            }
                            if (ds.child("player2").getValue().toString().equalsIgnoreCase(player.getName())) {
                                Log.i("PlayerSelectionActivity", "isHost" + isHost + " " + "isGameRoomCreated" + isGameRoomCreated);
                                if (isHost && isGameRoomCreated) {
                                    startGameBtn.setVisibility(View.VISIBLE);
                                }
                                if (ds.child("gameStatus").getValue().toString().equalsIgnoreCase("Started")) {
                                    Log.i("PlayerSelectionActivity", "GameStatus" + " " + "Started");
                                    Intent begin = new Intent(PlayerSelectionActivity.this, MultiplayerStartGameActivity.class);
                                    editor.putBoolean("isMultiplayer", true);
                                    editor.putBoolean("isHost", false);
                                    editor.putStringSet("gameChallenges", new HashSet<String>(gameChallenges));
                                    editor.putString("gameRoomKey", gameRoomKey);
                                    editor.commit();
                                    startActivity(begin);
                                }
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player = new Player();
                player.setName(playerName.getText().toString());
                if (player.getName() != "" && player.getName() != null) {
                    hostButton.setVisibility(View.VISIBLE);
                    participantButton.setVisibility(View.VISIBLE);
                    saveBtn.setVisibility(View.INVISIBLE);
                    playerName.setVisibility(View.INVISIBLE);
                }
            }
        });

        startGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Variables.timesPlayed = 1;
                gameData.child(gameRoomKey).child("gameStatus").setValue("Started");

            }
        });
    }
}
