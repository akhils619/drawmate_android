package com.akhil.drawmate.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.akhil.drawmate.R;
import com.akhil.drawmate.imageidentifiers.ImageIdentifier;
import com.akhil.drawmate.models.Results;
import com.akhil.drawmate.variables.Variables;
import com.rm.freedrawview.FreeDrawView;
import com.rm.freedrawview.PathDrawnListener;
import com.rm.freedrawview.PathRedoUndoCountChangeListener;
import com.rm.freedrawview.ResizeBehaviour;

import java.util.List;
import java.util.Locale;
import java.util.Random;

public class SinglePlayerGamePlayActivity extends AppCompatActivity implements TextToSpeech.OnInitListener{

    private List<Results> recognitionList;
    private ImageIdentifier imageClassifier;
    private String toBePredicted;
    private TextToSpeech textToSpeech;
    private final int CHALLENGE_COUNT = 4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_play);

        Intent prev = getIntent();
        Bundle b = prev.getExtras();
        textToSpeech = new TextToSpeech(this, this);
        TextView displayText = findViewById(R.id.predictionStringView);
        if (b != null) {
            this.toBePredicted = b.getString("givenLabel");
        } else {
            this.toBePredicted = "None";
        }
        final String drawText = "Draw " + toBePredicted;
        displayText.setText(drawText);
        init();

        final int[] time = {25};

        final CountDownTimer timer = new CountDownTimer(25000, 1000) {

            public void onTick(long millisUntilFinished) {
                TextView textTimer = (TextView) findViewById(R.id.timerView);
                String set = "00:" + checkDigit(time[0]);
                textTimer.setText(set);
                time[0]--;
            }

            public void onFinish() {
                TextView textTimer = (TextView) findViewById(R.id.timerView);
                String time_up = "Time's UP!";
                textTimer.setText(time_up);
                StartSinglePlayerGameActivity.gameHelper.getRandomLabel();
                if (Variables.timesPlayed == CHALLENGE_COUNT) {
                    Intent go = new Intent(SinglePlayerGamePlayActivity.this, SinglePlayerGameResultActivity.class);
                    startActivity(go);
                } else {
                    finish();
                }
            }

        };

        timer.start();

        final FreeDrawView drawImpressionView = (FreeDrawView) findViewById(R.id.freedraw_canvas);

        drawImpressionView.setPaintColor(Color.BLACK);
        drawImpressionView.setPaintWidthPx(getResources().getDimensionPixelSize(R.dimen.paint_width));
        drawImpressionView.setPaintWidthDp(getResources().getDimension(R.dimen.paint_width));
        drawImpressionView.setPaintAlpha(255);
        drawImpressionView.setResizeBehaviour(ResizeBehaviour.CROP);// Must be one of ResizeBehaviour
        drawImpressionView.setPathRedoUndoCountChangeListener(new PathRedoUndoCountChangeListener() {
            @Override
            public void onUndoCountChanged(int undoCount) {
            }
            @Override
            public void onRedoCountChanged(int redoCount) {
            }
        });
        drawImpressionView.setOnPathDrawnListener(new PathDrawnListener() {
            @Override
            public void onNewPathDrawn() {
                final FreeDrawView impressionView = drawImpressionView;
                impressionView.getDrawScreenshot(new FreeDrawView.DrawCreatorListener() {
                    @Override
                    public void onDrawCreated(Bitmap draw) {
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                                draw, ImageIdentifier.inputSize, ImageIdentifier.inputSize, false);
                        recognitionList = imageClassifier.recognizeImage(resizedBitmap);
                        String sen = sayItOut();
                        TextView displayText = (TextView) findViewById(R.id.predictionStringView);
                        displayText.setText(sen);
                        speakSuggestion(sen);
                        if (toBePredicted.equals(String.valueOf(recognitionList.get(0)))) {
                            Variables.correctGuesses++;
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    StartSinglePlayerGameActivity.gameHelper.getRandomLabel();
                                    timer.cancel();
                                    if (Variables.timesPlayed == CHALLENGE_COUNT) {
                                        Intent go = new Intent(SinglePlayerGamePlayActivity.this, SinglePlayerGameResultActivity.class);
                                        startActivity(go);
                                    } else {
                                        finish();
                                    }
                                }
                            }, 2000);
                        }
                    }

                    @Override
                    public void onDrawCreationError() {
                        Toast.makeText(SinglePlayerGamePlayActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onPathStart() {
            }
        });

        Button clear = (Button) findViewById(R.id.btn_clear_all);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FreeDrawView drawView = drawImpressionView;
                drawView.clearDrawAndHistory();
                TextView displayText = (TextView) findViewById(R.id.predictionStringView);
                displayText.setText(drawText);
            }
        });

        clear.performClick();
        Button undo = (Button) findViewById(R.id.btn_undo);
        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FreeDrawView drawView = drawImpressionView;
                drawView.undoLast();
            }
        });

        Button redo = (Button) findViewById(R.id.btn_redo);
        redo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FreeDrawView drawView = drawImpressionView;
                drawView.redoLast();
            }
        });
    }

    public void init() {
        imageClassifier = new ImageIdentifier(SinglePlayerGamePlayActivity.this);
    }

    private String sayItOut() {
        String[] patterns = {"I see ", "Oh I know this. This is ", "Umm, this looks like ", "Kind of looks like ", "Is this ", "Is this really how you draw ", "This looks awful, but it may be "};
        Random generator = new Random();
        int rnd_index = generator.nextInt(patterns.length);
        String sentence_select = patterns[rnd_index];
        String sentence = sentence_select + recognitionList.get(0);
        return sentence;
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = textToSpeech.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }

    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        if (Variables.timesPlayed == CHALLENGE_COUNT) {
            Intent go = new Intent(SinglePlayerGamePlayActivity.this, SinglePlayerGameResultActivity.class);
            startActivity(go);
        } else {
            finish();
        }
        super.onDestroy();
    }

    private void speakSuggestion(String text) {
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }


}
