package com.akhil.drawmate.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.akhil.drawmate.R;
import com.akhil.drawmate.models.GameData;


public class MainMenuActivity extends AppCompatActivity {

    private GameData game;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_side_menu);
        game = new GameData();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        LottieAnimationView animationView = findViewById(R.id.animation);
        animationView.setAnimation("animations/drawAnim.json");
        animationView.playAnimation();
        animationView.loop(true);

        Button startSiglePlayerGame = (Button) findViewById(R.id.startGame);
        startSiglePlayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent begin = new Intent(MainMenuActivity.this, StartSinglePlayerGameActivity.class);
                startActivity(begin);
            }
        });

        Button startMultiplayerGame = (Button) findViewById(R.id.startmultiplayergame);
        startMultiplayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent begin = new Intent(MainMenuActivity.this, PlayerSelectionActivity.class);
                begin.putExtra("isMultiplayer", true);
                startActivity(begin);
            }
        });

        Button exitGame = (Button) findViewById(R.id.exitGame);
        exitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitGame();
            }
        });
    }

    private void exitGame() {
        new AlertDialog.Builder(MainMenuActivity.this)
                .setTitle("Exit?")
                .setMessage("Are you sure you want to close ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        MainMenuActivity.this.finish();
                        System.exit(0);
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void onBackPressed() {
        exitGame();
    }

}
