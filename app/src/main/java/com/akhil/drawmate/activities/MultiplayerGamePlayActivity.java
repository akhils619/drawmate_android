package com.akhil.drawmate.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.akhil.drawmate.R;
import com.akhil.drawmate.imageidentifiers.ImageIdentifier;
import com.akhil.drawmate.models.Results;
import com.akhil.drawmate.variables.Variables;
import com.rm.freedrawview.FreeDrawView;
import com.rm.freedrawview.PathDrawnListener;
import com.rm.freedrawview.PathRedoUndoCountChangeListener;
import com.rm.freedrawview.ResizeBehaviour;

import java.util.List;
import java.util.Locale;
import java.util.Random;

public class MultiplayerGamePlayActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private List<Results> recognitionList;
    private ImageIdentifier imageIdentifier;
    private String toBePredicted;
    private TextToSpeech textToSpeech;
    private final int CHALLENGE_COUNT = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_play);
        Intent previousIntent = getIntent();
        Bundle b = previousIntent.getExtras();
        TextView displayText = findViewById(R.id.predictionStringView);
        if (b != null) {
            this.toBePredicted = b.getString("givenLabel");
        } else {
            this.toBePredicted = "None";
        }
        textToSpeech = new TextToSpeech(this, this);

        final String drawText =  toBePredicted;
        displayText.setText(drawText);
        init();
        final int[] time = {25};

        final CountDownTimer timer = new CountDownTimer(25000, 1000) {
            public void onTick(long millisUntilFinished) {
                TextView textTimer = (TextView) findViewById(R.id.timerView);
                String set = "00:" + checkDigit(time[0]);
                textTimer.setText(set);
                time[0]--;
            }

            public void onFinish() {
                TextView textTimer = (TextView) findViewById(R.id.timerView);
                String time_up = "Time's UP!";
                textTimer.setText(time_up);
                if (Variables.timesPlayed == CHALLENGE_COUNT) {
                    Intent go = new Intent(MultiplayerGamePlayActivity.this, MultiplayerGameResultActivity.class);
                    startActivity(go);
                } else {
                    finish();
                }
            }
        };
        timer.start();

        final FreeDrawView drawImpressionView = (FreeDrawView) findViewById(R.id.freedraw_canvas);
        drawImpressionView.setPaintColor(Color.BLACK);
        drawImpressionView.setPaintWidthPx(getResources().getDimensionPixelSize(R.dimen.paint_width));
        drawImpressionView.setPaintWidthDp(getResources().getDimension(R.dimen.paint_width));
        drawImpressionView.setPaintAlpha(255);
        drawImpressionView.setResizeBehaviour(ResizeBehaviour.CROP);
        drawImpressionView.setPathRedoUndoCountChangeListener(new PathRedoUndoCountChangeListener() {
            @Override
            public void onUndoCountChanged(int undoCount) {
            }

            @Override
            public void onRedoCountChanged(int redoCount) {
            }
        });
        drawImpressionView.setOnPathDrawnListener(new PathDrawnListener() {
            @Override
            public void onNewPathDrawn() {
                final FreeDrawView impressionView = drawImpressionView;
                impressionView.getDrawScreenshot(new FreeDrawView.DrawCreatorListener() {
                    @Override
                    public void onDrawCreated(Bitmap draw) {
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                                draw, ImageIdentifier.inputSize, ImageIdentifier.inputSize, false);
                        recognitionList = imageIdentifier.recognizeImage(resizedBitmap);
                        String loudSuggestion = saySuggestion();
                        TextView displayText =  findViewById(R.id.predictionStringView);
                        displayText.setText(loudSuggestion);
                        speakSuggestion(loudSuggestion);
                        Log.i("MultiplayerGamePlay", "To be predicted: "+ toBePredicted);
                        Log.i("MultiplayerGamePlay", "Output from Tensorflow: "+ recognitionList.get(0));

                        if (toBePredicted.equalsIgnoreCase(String.valueOf(recognitionList.get(0)))) {
                            Variables.correctGuesses++;
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    MultiplayerStartGameActivity.gameHelper.getMultiplayerLabels();
                                    timer.cancel();
                                    if (Variables.timesPlayed == CHALLENGE_COUNT) {
                                        Intent go = new Intent(MultiplayerGamePlayActivity.this, MultiplayerGameResultActivity.class);
                                        startActivity(go);
                                    } else {
                                        finish();
                                    }
                                }
                            }, 2000);
                        }
                    }

                    @Override
                    public void onDrawCreationError() {
                        Toast.makeText(MultiplayerGamePlayActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onPathStart() {
            }
        });

        Button clear = (Button) findViewById(R.id.btn_clear_all);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FreeDrawView sigView = drawImpressionView;
                sigView.clearDrawAndHistory();
                TextView displayText = (TextView) findViewById(R.id.predictionStringView);
                displayText.setText(drawText);
            }
        });

        clear.performClick();
        Button undo = (Button) findViewById(R.id.btn_undo);
        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FreeDrawView drawView = drawImpressionView;
                drawView.undoLast();
            }
        });

        Button redo = (Button) findViewById(R.id.btn_redo);
        redo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FreeDrawView sigView = drawImpressionView;
                sigView.redoLast();
            }
        });
    }

    public void init() {
        imageIdentifier = new ImageIdentifier(MultiplayerGamePlayActivity.this);
    }

    private String saySuggestion() {
        String[] patterns = {"I see ", "Oh I know this. This is ", "Umm, this looks like ", "Kind of looks like ", "Is this ", "Is this really how you draw ", "This looks awful, but it may be "};
        Random generator = new Random();
        int rnd_index = generator.nextInt(patterns.length);
        String sentence_select = patterns[rnd_index];
        String sentence = sentence_select + recognitionList.get(0);
        return sentence;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    private void speakSuggestion(String text) {
        if (!text.equalsIgnoreCase("") && text != null) {
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = textToSpeech.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TextToSpeech", "This Language is not supported");
            }
        } else {
            Log.e("TextToSpeech", "Initilization Failed!");
        }
    }
}
