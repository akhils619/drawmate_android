package com.akhil.drawmate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.akhil.drawmate.R;
import com.akhil.drawmate.helpers.GameHelper;


public class StartSinglePlayerGameActivity extends AppCompatActivity {

    public static GameHelper gameHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start_game);
        LinearLayout linearLayout = findViewById(R.id.layout_start_game);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoInGameActivity();
            }
        });
        gameHelper = GameHelper.getInstance(StartSinglePlayerGameActivity.this);


        TextView newLabelView =  findViewById(R.id.newLabelView);
        String set = setNewLabel();
        newLabelView.setText(set);

        TextView displayNumber =  findViewById(R.id.drawingCount);
        String dis = "Drawing " + gameHelper.getPlayNumber() + " / 4";
        displayNumber.setText(dis);

        Button startGame = findViewById(R.id.startGameBtn);
        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoInGameActivity();
            }
        });
    }

    private void gotoInGameActivity() {
        gameHelper.incrementPlayNumber();
        Intent startg = new Intent(StartSinglePlayerGameActivity.this, SinglePlayerGamePlayActivity.class);
        startg.putExtra("givenLabel", gameHelper.getGivenLabel());
        startActivity(startg);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Intent restart = new Intent(StartSinglePlayerGameActivity.this, StartSinglePlayerGameActivity.class);
        startActivity(restart);
        finish();
    }

    private String setNewLabel() {
        String random_label = gameHelper.getRandomLabel();
        String set = "Draw \n" + random_label + "\n in under 25 seconds";
        return set;
    }


}
