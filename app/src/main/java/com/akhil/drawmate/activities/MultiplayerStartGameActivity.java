package com.akhil.drawmate.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.akhil.drawmate.R;
import com.akhil.drawmate.helpers.GameHelper;
import com.akhil.drawmate.models.GameData;
import com.akhil.drawmate.variables.Variables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MultiplayerStartGameActivity extends AppCompatActivity {

    public static GameHelper gameHelper;
    public GameData game;
    ArrayList<String> gameChallenges;
    SharedPreferences preferences;
    Set<String> challengeSet;
    int count = 1;
    private String challenge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);

        LinearLayout linearLayout = findViewById(R.id.layout_start_game);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoInGameActivity();
            }
        });
        gameHelper = GameHelper.getInstance(this);
        challengeSet = new HashSet<String>();
        gameChallenges = new ArrayList<String>();
        TextView newLabelView = findViewById(R.id.newLabelView);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        challengeSet = preferences.getStringSet("gameChallenges", new HashSet<String>());
        gameChallenges.addAll(challengeSet);
        Log.i("MultiplayerStartGame", "Multiplayer is enabled");
        challenge = setMultiplayerLabel();
        newLabelView.setText(  "Draw \n" + challenge + "\n in under 25 seconds");

        TextView displayNumber = findViewById(R.id.drawingCount);
        String dis = "Drawing " + gameHelper.getPlayNumber() + " / 5";
        displayNumber.setText(dis);

        Button startGame = findViewById(R.id.startGameBtn);
        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoInGameActivity();
            }
        });
    }

    private void gotoInGameActivity() {
        gameHelper.incrementPlayNumber();
        Intent startg = new Intent(MultiplayerStartGameActivity.this, MultiplayerGamePlayActivity.class);
        Variables.alreadyDone.add(challenge);
        startg.putExtra("givenLabel", challenge);
        startActivity(startg);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Intent restart = new Intent(MultiplayerStartGameActivity.this, MultiplayerStartGameActivity.class);
        startActivity(restart);
        finish();
    }

    private String setMultiplayerLabel() {
        Log.i("MultiplayerStartGame", "Inside set Multiplayer label");
        Collections.sort(gameChallenges);
//        Random generator = new Random();
//        int randomIndex = generator.nextInt(gameChallenges.size());
//        String label = gameChallenges.get(randomIndex);
//        while (Variables.alreadyDone.contains(label)) {
//            randomIndex = generator.nextInt(gameChallenges.size());
//            label = gameChallenges.get(randomIndex);
//        }
        String label = gameChallenges.get(Variables.timesPlayed);
        return label;
    }


}
