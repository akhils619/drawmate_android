package com.akhil.drawmate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.akhil.drawmate.R;
import com.akhil.drawmate.variables.Variables;

public class SinglePlayerGameResultActivity extends AppCompatActivity {

    private final int CHALLENGE_COUNT = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_result);

        TextView displayView = findViewById(R.id.numGuess);
        String set = "Guessed " + Variables.correctGuesses + " /"+ CHALLENGE_COUNT + "correctly!";
        displayView.setText(set);

        Button playAgain =  findViewById(R.id.playAgain);
        playAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Variables.timesPlayed = 1;
                Variables.correctGuesses = 0;
                Variables.alreadyDone.clear();
                startActivity(new Intent(SinglePlayerGameResultActivity.this, MainMenuActivity.class));
            }
        });

        Button mainMenu = (Button) findViewById(R.id.goToMenu);
        mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SinglePlayerGameResultActivity.this, MainMenuActivity.class));
                finish();
            }
        });
    }
}