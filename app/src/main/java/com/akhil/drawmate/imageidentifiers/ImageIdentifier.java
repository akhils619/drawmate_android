package com.akhil.drawmate.imageidentifiers;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Vector;

import org.tensorflow.Operation;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import com.akhil.drawmate.helpers.ImageConverter;
import com.akhil.drawmate.models.Results;

public class ImageIdentifier {

    public static final int inputSize = 224;
    private static final float THRESHOLD = 0.1f;
    private static final int maxResults = 3;
    private static final String MODEL_FILE = "retrained_graph.pb";
    private static final String LABEL_FILE = "retrained_labels.txt";
    private static final int CLASS_SIZE = 91;
    private static final String INPUT_NAME = "input";
    private static final String OUTPUT_NAME = "final_result";
    private static final String[] OUTPUT_NAMES = {OUTPUT_NAME};

    private Context context;
    private Operation output_op;
    private TensorFlowInferenceInterface tfInterface;
    private Vector<String> labels;

    public ImageIdentifier(Context context) {
        this.context = context;
        this.tfInterface = new TensorFlowInferenceInterface(context.getAssets(), MODEL_FILE);
        InitLabels();
    }

    private void InitLabels() {
        labels = new Vector<>(CLASS_SIZE);
        try {
            BufferedReader br = null;
            InputStream stream = context.getAssets().open(LABEL_FILE);
            br = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = br.readLine()) != null) {
                labels.add(line);
            }
            br.close();
        } catch (IOException e) {
            throw new RuntimeException("Error reading label file!", e);
        }
    }

    public List<Results> recognizeImage(final Bitmap bitmap) {
        return recognizeImage(ImageConverter.bitmapToFloat(bitmap));
    }

    public List<Results> recognizeImage(final float[] imageFloats) {

        this.tfInterface.feed(INPUT_NAME, imageFloats, 1, inputSize, inputSize, 3);
        this.tfInterface.run(OUTPUT_NAMES, false);
        float[] outputs = new float[CLASS_SIZE];
        this.tfInterface.fetch(OUTPUT_NAME, outputs);

        PriorityQueue<Results> pq =
                new PriorityQueue<>(
                        3,
                        new Comparator<Results>() {
                            @Override
                            public int compare(Results lhs, Results rhs) {
                                return Float.compare(rhs.getConfidence(), lhs.getConfidence());
                            }
                        });

        for (int i = 0; i < outputs.length; ++i) {
            if (outputs[i] > THRESHOLD) {
                pq.add(new Results("" + i, labels.get(i), outputs[i], null));
            }
        }

        final ArrayList<Results> recognitions = new ArrayList<>();
        int recognitionsSize = Math.min(pq.size(), maxResults);
        for (int i = 0; i < recognitionsSize; ++i) {
            recognitions.add(pq.poll());
        }
        return recognitions;
    }

}
