package com.akhil.drawmate.models;

public class Player {

    String name;
    int score;
    boolean isHost;

    public Player(String name, int score, boolean isHost) {
        this.name = name;
        this.score = score;
        this.isHost = isHost;
    }

    public Player(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isHost() {
        return isHost;
    }

    public void setHost(boolean host) {
        isHost = host;
    }


}
