package com.akhil.drawmate.models;

import android.annotation.SuppressLint;
import android.graphics.RectF;

/**
 * Created by Akhil Somaraj on 03/12/2019.
 */
public class Results {

    private final String id;
    private final String title;
    private final Float confidence;
    private RectF location;

    public Results(
            final String id, final String title, final Float confidence, final RectF location) {
        this.id = id;
        this.title = title;
        this.confidence = confidence;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Float getConfidence() {
        return confidence;
    }

    public RectF getLocation() {
        return new RectF(location);
    }

    public void setLocation(RectF location) {
        this.location = location;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public String toString() {
        String resultString = "";
        if (id != null) {
            //resultString += "[" + id + "] ";
        }

        if (title != null) {
            resultString += title;
        }

        if (confidence != null) {
            //resultString += "(Confidence :" + String.format(" %.1f%% ", confidence * 100.0f) + ")";
        }

        if (location != null) {
            //resultString += location + " ";
        }

        return resultString.trim();
    }
}
