package com.akhil.drawmate.models;

import java.util.ArrayList;
import java.util.List;

public class GameData {

    String player1;
    String player2;
    String gameStatus;
    int scoreP1;
    int scoreP2;
    int timeP1;
    int timeP2;
    ArrayList<String> gameChallenges;

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public int getScoreP1() {
        return scoreP1;
    }

    public void setScoreP1(int scoreP1) {
        this.scoreP1 = scoreP1;
    }

    public int getScoreP2() {
        return scoreP2;
    }

    public void setScoreP2(int scoreP2) {
        this.scoreP2 = scoreP2;
    }

    public int getTimeP1() {
        return timeP1;
    }

    public void setTimeP1(int timeP1) {
        this.timeP1 = timeP1;
    }

    public int getTimeP2() {
        return timeP2;
    }

    public void setTimeP2(int timeP2) {
        this.timeP2 = timeP2;
    }

    public List<String> getGameChallenges() {
        return gameChallenges;
    }

    public void setGameChallenges(ArrayList<String> gameChallenges) {
        this.gameChallenges = gameChallenges;
    }

    public GameData(){

    }

    public GameData(String player1, String player2, String gameStatus, int scoreP1, int scoreP2, int timeP1, int timeP2, ArrayList<String> gameChallenges) {
        this.player1 = player1;
        this.player2 = player2;
        this.gameStatus = gameStatus;
        this.scoreP1 = scoreP1;
        this.scoreP2 = scoreP2;
        this.timeP1 = timeP1;
        this.timeP2 = timeP2;
        this.gameChallenges = gameChallenges;
    }




}
