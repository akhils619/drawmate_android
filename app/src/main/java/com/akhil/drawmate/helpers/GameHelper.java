package com.akhil.drawmate.helpers;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import com.akhil.drawmate.variables.Variables;

public class GameHelper {

    private static final String LABEL_FILE = "retrained_labels.txt";
    private static final int CLASS_SIZE = 99;
    Context context;
    private int correctGuesses;
    private int totalGuesses;
    private String givenLabel;
    private Vector<String> labels;
    public ArrayList<String> multiplayerLabels;
    //boolean isLAreadyExecuted;
    private static GameHelper gameHelper = null;
    private ArrayList<String> validChallenges = new ArrayList<>();


    private GameHelper(Context context) {
        this.context = context;
        totalGuesses = 1;
        givenLabel = "Start";
        getLabels();
        getMultiplayerChallenges();
        if (Variables.timesPlayed > 8) {
            Variables.timesPlayed = 1;
            Variables.correctGuesses = 0;
            Variables.alreadyDone.clear();
        }
    }

    public String getRandomLabel() {
        Random generator = new Random();
        int rnd_index = generator.nextInt(validChallenges.size());
        String label = validChallenges.get(rnd_index);
        while (Variables.alreadyDone.contains(label)) {
            rnd_index = generator.nextInt(validChallenges.size());
                label = validChallenges.get(rnd_index);
            }
        givenLabel = label;
        return label;
    }


    public String getMultiplayerLabels() {
        int count = 0;
        String label = multiplayerLabels.get(0);
        while (Variables.alreadyDone.contains(label)) {
            if (count < multiplayerLabels.size()) {
                label = multiplayerLabels.get(count);
                count++;
            }
        }
        givenLabel = label;
        return label;
    }

    private void getLabels() {
        labels = new Vector<>(CLASS_SIZE);
        try {
            BufferedReader br = null;
            InputStream stream = context.getAssets().open(LABEL_FILE);
            br = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = br.readLine()) != null) {
                labels.add(line);
            }
            br.close();
        } catch (IOException e) {
            throw new RuntimeException("Problem reading label file!", e);
        }
    }

    public ArrayList<String> getMultiplayerChallenges() {
        Log.i("GameHelper", "Inside get multiplayer challenges");
        Collections.addAll(validChallenges, Variables.voidChallenges);

        //isLAreadyExecuted = true;
        Random generator = new Random();
        int rnd_index = generator.nextInt(labels.size());
        ArrayList<String> gameChallenges = new ArrayList<String>();
        while (gameChallenges.size() <= 5) {
            rnd_index = generator.nextInt(labels.size());
            String label = labels.get(rnd_index);
            if (validChallenges.contains(label)) {
                gameChallenges.add(label);
            }
        }
        multiplayerLabels = gameChallenges;
        return multiplayerLabels;
    }

    public void setLabels(ArrayList<String> challenges) {
        this.multiplayerLabels = new ArrayList<String>();
        this.multiplayerLabels.addAll(challenges);
    }

    public static GameHelper getInstance(Context context) {
        if (gameHelper == null)
            gameHelper = new GameHelper(context);

        return gameHelper;
    }

    public String getGivenLabel() {
        return this.givenLabel;
    }

    public void incrementPlayNumber() {
        Variables.timesPlayed++;
    }

    public int getPlayNumber() {
        return Variables.timesPlayed;
    }

}
