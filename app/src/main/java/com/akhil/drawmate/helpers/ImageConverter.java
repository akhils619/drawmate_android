package com.akhil.drawmate.helpers;

import android.graphics.Bitmap;

public class ImageConverter {

    private static final int imageMean = 0;
    private static final float imageStd = 255;


    public static float[] bitmapToFloat(Bitmap bitmap) {
        int[] intValues = new int[bitmap.getWidth() * bitmap.getHeight()];
        float[] result = new float[bitmap.getWidth() * bitmap.getHeight() * 3];

        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        for (int i = 0; i < intValues.length; ++i) {
            final int val = intValues[i];
            result[i * 3] = (((val >> 16) & 0xFF) - imageMean) / imageStd;
            result[i * 3 + 1] = (((val >> 8) & 0xFF) - imageMean) / imageStd;
            result[i * 3 + 2] = ((val & 0xFF) - imageMean) / imageStd;
        }

        return result;
    }

}
